<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <#if usuario == false>
        <#include "/navbar.ftl">
    </#if>
    <#if usuario != false>
        <#include "/navBarLogOut.ftl">
    </#if>
    <div class="jumbotron">
        <h1 class="display-4">${articulo.titulo}</h1>
        <p class="lead">${articulo.cuerpo}</p>
        <hr class="my-4">
        <p>${articulo.autor} | ${articulo.fecha}</p>
        <#list articulo.etiquetas as articulo>
            <button type="button" class="btn btn-outline-secondary">${articulo.etiqueta}</button>
        </#list>
        <#if esAdministrador == true || esMismoAutor == true>
            <a href="/editarArticulo/${articulo.codigo}"><button type="button" class="btn btn-primary btn-lg btn-block">EDITAR</button></a>
            <a href="/eliminar/${articulo.codigo}"><button type="button" class="btn btn-danger btn-lg btn-block">ELIMINAR</button></a>
        </#if>
        <#if usuario == true>
            <form action="/comentar/${articulo.codigo}" method="post">
                <div class="form-group">
                    <label for="comentario">Comentario</label>
                    <textarea class="form-control" id="comentario" name="comentario" rows="5"></textarea>
                    <button type="submit" class="btn btn-secondary btn-lg btn-block">COMENTAR</button>
                </div>
            </form>
        </#if>
            <#list articulo.comentarios as coment>
                <div class="card text-white bg-dark mb-3" style="max-width: 100%;">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <h5 class="card-title">${coment.autor}</h5>
                        <p class="card-text">${coment.comentario}</p>
                        <#if esAdministrador == true>
                            <a href="/eliminarComentario/${coment.codigo}/${articulo.codigo}"><button type="button" class="btn btn-danger btn-lg btn-block">ELIMINAR</button></a>
                        </#if>
                    </div>
                </div>
            </#list>
    </div>
</body>
</html>
