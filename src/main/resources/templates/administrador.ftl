<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <#include "/navbarAdmin.ftl">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <form action="/agregarNuevoUsuario/" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="usuarioNombre">Usuario</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Usuario" name="usuario" aria-label="usuario" aria-describedby="usuarioNombre">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="nombreSpan">Nombre</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Nombre" name="nombre" aria-label="Nombre" aria-describedby="nombreSpan">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="contraseniaSpan">Contraseña</span>
                    </div>
                    <input type="password" class="form-control" placeholder="Contraseña" name="contrasenia" aria-label="Contraseña" aria-describedby="contraseniaSpan">
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="administradorCheck" name="administrador" value="true">
                    <label class="form-check-label" for="administradorCheck">Administrador</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="autorCheck" name="autor" value="true">
                    <label class="form-check-label" for="autorCheck">Autor</label>
                </div>
                <button type="submit" class="btn btn-dark">Agregar</button>
            </form>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <form action="/agregarArticuloAdmin/" method="post">
                <div class="form-group">
                    <label for="titulo">Título</label>
                    <input type="text" class="form-control" name="titulo" placeholder="Título">
                </div>
                <div class="form-group">
                    <label for="etiquetas">Etiquetas</label>
                    <input type="text" class="form-control" name="etiquetas" placeholder="Etiquetas">
                </div>
                <div class="form-group">
                    <label for="cuerpoArticulo">Cuerpo Articulo</label>
                    <textarea class="form-control" name="cuerpoArticulo" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-secondary btn-lg btn-block">INSERTAR ENTRADA EN EL BLOG</button>
            </form>
        </div>
    </div>
</body>
</html>