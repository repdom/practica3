<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Entradas del blog</title>
</head>
<body>
    <#if usuario == "null">
        <#include "/navbar.ftl">
    </#if>
    <#if usuario != "null">
        <#include "/navBarLogOut.ftl">
    </#if>
    <#list articulos as art>
        <div class="jumbotron">
            <h1 class="display-4">${art.titulo}</h1>
            <p class="lead">${art.cuerpo}...</p>
            <hr class="my-4">
            <p>${art.autor}</p>
            <a class="btn btn-primary btn-lg" href="/articulo/${art.codigo}" role="button">Leer Más</a>
        </div>
    </#list>
    <#if esAdmin == true>
        <a href="/administrador/${usuario}"><button type="button" class="btn btn-primary btn-lg btn-block">PANEL ADMINISTRADOR</button></a>
    </#if>
</body>
</html>