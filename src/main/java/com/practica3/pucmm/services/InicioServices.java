package com.practica3.pucmm.services;

import org.h2.tools.Server;

import java.sql.*;

import static com.practica3.pucmm.services.DataBaseServices.*;


public class InicioServices {
    public static void iniciarDb() throws SQLException {
        Server.createTcpServer("-tcpPort", "9092", "-tcpAllowOthers").start();
    }

    public static void stopDb() throws SQLException {
        Server.shutdownTcpServer("tcp://localhost:9092", "", true, true);
    }

    public static void crearTablas() throws SQLException {
        // Connection con = DataBaseServices.getConexion();

        String sql = "CREATE TABLE IF NOT EXISTS USUARIO (\n" +
                    " NOMBRE_USUARIO VARCHAR(124) PRIMARY KEY NOT NULL,\n" +
                    " NOMBRE VARCHAR(100) NOT NULL,\n" +
                    " PASSWORD VARCHAR(100) NOT NULL,\n" +
                    " ADMINISTRADOR BOOL NOT NULL,\n" +
                    " AUTOR BOOL NOT NULL,\n" +
                ");";
        sql += "\nCREATE TABLE IF NOT EXISTS ARTICULO\n" +
                "(\n" +
                    " CODIGO INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    " TITULO VARCHAR(464) NOT NULL,\n" +
                    " CUERPO VARCHAR(10156) NOT NULL,\n" +
                    " FECHA_PUBLICACION DATETIME NOT NULL,\n" +
                    " AUTOR VARCHAR(456) NOT NULL,\n" +
                ");\n";
        sql += "\nCREATE TABLE IF NOT EXISTS ETIQUETA\n" +
                "(\n" +
                    " CODIGO INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    " NOMBRE VARCHAR(256) NOT NULL,\n" +
                    " ARTICULO INTEGER NOT NULL,\n" +
                ");";
        sql += "\nCREATE TABLE IF NOT EXISTS COMENTARIO\n" +
                "(\n" +
                    " CODIGO INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    " COMENTARIO VARCHAR(4096) NOT NULL,\n" +
                    " ARTICULO INTEGER NOT NULL,\n" +
                    " AUTOR VARCHAR(456) NOT NULL,\n" +
                ");";
        sql += "\nALTER TABLE COMENTARIO ADD FOREIGN KEY (AUTOR) REFERENCES USUARIO(NOMBRE_USUARIO);";
        sql += "\nALTER TABLE ARTICULO ADD FOREIGN KEY (AUTOR) REFERENCES USUARIO(NOMBRE_USUARIO);";
        sql += "\nALTER TABLE COMENTARIO ADD FOREIGN KEY (ARTICULO) REFERENCES ARTICULO(CODIGO);";
        sql += "\nALTER TABLE ETIQUETA ADD FOREIGN KEY (ARTICULO) REFERENCES ARTICULO(CODIGO);";
        sql += "\nMERGE INTO USUARIO KEY (NOMBRE_USUARIO) VALUES ('repdom', 'Juan T. Ángel', 'RDsIdEe1865@', TRUE, TRUE);";

        Connection con = DataBaseServices.getInstance().getConexion();
        Statement stm = con.createStatement();
        stm.execute(sql);

        stm.close();
        con.close();
    }
}
