package com.practica3.pucmm.services;

import com.practica3.pucmm.clases.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioServices {
    public Usuario getUsuario(String nombreUsuario) {
        Usuario usuario = null;
        Connection con = null;

        try {
            String consultaSql = "SELECT * FROM USUARIO WHERE NOMBRE_USUARIO = ?";
            con = DataBaseServices.getInstance().getConexion();

            PreparedStatement preparedStatement = con.prepareStatement(consultaSql);
            preparedStatement.setString(1, nombreUsuario);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                usuario = new Usuario();
                usuario.setAdministrador(rs.getBoolean("ADMINISTRADOR"));
                usuario.setAutor(rs.getBoolean("AUTOR"));
                usuario.setNombre(rs.getString("NOMBRE"));
                usuario.setPassword(rs.getString("PASSWORD"));
                usuario.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuario;
    }
    public boolean crearUsuario(Usuario usuario){
        boolean ok =false;

        Connection con = null;
        try {

            String query = "INSERT INTO USUARIO(NOMBRE_USUARIO, NOMBRE, PASSWORD, ADMINISTRADOR, AUTOR) values(?,?,?,?,?)";
            con = DataBaseServices.getInstance().getConexion();
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, usuario.getNombreUsuario());
            prepareStatement.setString(2, usuario.getNombre());
            prepareStatement.setString(3, usuario.getPassword());
            prepareStatement.setBoolean(4, usuario.isAdministrador());
            prepareStatement.setBoolean(5, usuario.isAutor());
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return ok;
    }

    public boolean actualizarUsuario(Usuario usuario){
        boolean ok =false;

        Connection con = null;
        try {

            String query = "UPDATE USUARIO SET NOMBRE=?, PASSWORD=?, ADMINISTRADOR=?, AUTOR=? WHERE NOMBRE_USUARIO = ?";
            con = DataBaseServices.getInstance().getConexion();
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, usuario.getNombre());
            prepareStatement.setString(2, usuario.getPassword());
            prepareStatement.setBoolean(3, usuario.isAdministrador());
            prepareStatement.setBoolean(4, usuario.isAutor());
            //Indica el where...
            prepareStatement.setString(5, usuario.getNombreUsuario());
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return ok;
    }

    public boolean borrarUsuario(String nombreDeUsuario){
        boolean ok =false;

        Connection con = null;
        try {

            String query = "DELETE FROM USUARIO WHERE NOMBRE_USUARIO = ?";
            con = DataBaseServices.getInstance().getConexion();
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);

            //Indica el where...
            prepareStatement.setString(1, nombreDeUsuario);
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ok;
    }
}
