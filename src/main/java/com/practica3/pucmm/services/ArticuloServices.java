package com.practica3.pucmm.services;

import com.practica3.pucmm.clases.Articulo;
import com.practica3.pucmm.clases.Comentario;
import com.practica3.pucmm.clases.Etiqueta;
import com.practica3.pucmm.clases.Usuario;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class ArticuloServices {

    public ArrayList<Articulo> listaArticulos() {
        ArrayList<Articulo> lista = new ArrayList<>();
        Connection con = null; //objeto conexion.
        try {
            //
            String query = "SELECT * FROM ARTICULO ORDER BY FECHA_PUBLICACION DESC";
            con = DataBaseServices.getInstance().getConexion(); //referencia a la conexion.
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);
            ResultSet rs = prepareStatement.executeQuery();
            while(rs.next()){
                Articulo art = new Articulo();
                art.setCodigo(rs.getInt("CODIGO"));
                art.setTitulo(rs.getString("TITULO"));
                art.setCuerpo(rs.getString("CUERPO").substring(0, 70));
                art.setFecha(rs.getDate("FECHA_PUBLICACION"));
                art.setAutor(rs.getString("AUTOR"));
                System.out.println(art.getTitulo());
                lista.add(art);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return lista;
    }

    public boolean crearComentario(Comentario comentario, int articulo) {
        boolean ok =false;

        Connection con = null;
        try {
            String query = "INSERT INTO COMENTARIO(COMENTARIO, ARTICULO, AUTOR) values(?,?,?)";
            con = DataBaseServices.getInstance().getConexion();
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, comentario.getComentario());
            prepareStatement.setInt(2, articulo);
            prepareStatement.setString(3, comentario.getAutor());
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return ok;
    }

    public Articulo getArticulo(int codigoArticulo) {
        Articulo articulo = null;
        Connection con = null;

        try {
            String consultaSql = "SELECT * FROM ARTICULO WHERE CODIGO = ?";
            con = DataBaseServices.getInstance().getConexion();

            PreparedStatement preparedStatement = con.prepareStatement(consultaSql);
            preparedStatement.setInt(1, codigoArticulo);
            ResultSet rs = preparedStatement.executeQuery();
            articulo = new Articulo();
            while (rs.next()) {
                articulo.setCodigo(codigoArticulo);
                articulo.setTitulo(rs.getString("TITULO"));
                articulo.setCuerpo(rs.getString("CUERPO"));
                articulo.setFecha(rs.getDate("FECHA_PUBLICACION"));
                articulo.setAutor(rs.getString("AUTOR"));
            }
            String consultaSqlAtributo = "SELECT * FROM ETIQUETA WHERE ARTICULO = ?;";
            PreparedStatement preparedStatementAtributo = con.prepareStatement(consultaSqlAtributo);
            preparedStatementAtributo.setInt(1, codigoArticulo);
            ResultSet rsArticulo = preparedStatementAtributo.executeQuery();
            ArrayList<Etiqueta> etiquetas = new ArrayList<>();
            while (rs.next()) {
                Etiqueta atq = new Etiqueta();
                atq.setCodigo(rs.getInt("CODIGO"));
                atq.setEtiqueta(rs.getString("NOMBRE"));
                etiquetas.add(atq);
            }
            String consultaSqlComentario = "SELECT * FROM COMENTARIO WHERE ARTICULO = ?";
            PreparedStatement preparedStatementComentario = con.prepareStatement(consultaSqlComentario);
            preparedStatementComentario.setInt(1, codigoArticulo);
            ResultSet rsComentario = preparedStatementComentario.executeQuery();
            ArrayList<Comentario> comentarios = new ArrayList<>();
            while(rsComentario.next()) {
                Comentario com = new Comentario();
                com.setArticulo(articulo);
                com.setAutor(rsComentario.getString("AUTOR"));
                com.setCodigo(rsComentario.getInt("CODIGO"));
                com.setComentario(rsComentario.getString("COMENTARIO"));
                comentarios.add(com);
            }
            articulo.setComentarios(comentarios);
            articulo.setEtiquetas(etiquetas);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articulo;
    }
    public int crearArticulo(Articulo articulo){
        boolean ok =false;
        int codigo = 0;

        Connection con = null;
        try {

            String query = "INSERT INTO ARTICULO(TITULO, CUERPO, FECHA_PUBLICACION, AUTOR) values(?,?,?,?)";
            con = DataBaseServices.getInstance().getConexion();
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, articulo.getTitulo());
            prepareStatement.setString(2, articulo.getCuerpo());
            prepareStatement.setDate(3, articulo.getFecha());
            prepareStatement.setString(4, articulo.getAutor());
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

            if(ok) {
                String queryCodigo = "SELECT CODIGO FROM ARTICULO WHERE TITULO = ?";
                PreparedStatement preparedStatementParaCodigo = con.prepareStatement(queryCodigo);
                preparedStatementParaCodigo.setString(1, articulo.getTitulo());
                ResultSet rs = preparedStatementParaCodigo.executeQuery();
                while (rs.next()) {
                    codigo = rs.getInt("CODIGO");
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return codigo;
    }

    public boolean crearEtiqueta(Etiqueta etiqueta) {
        boolean ok =false;

        Connection con = null;
        try {
            String query = "INSERT INTO ETIQUETA(NOMBRE, ARTICULO) values(?,?)";
            con = DataBaseServices.getInstance().getConexion();
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, etiqueta.getEtiqueta());
            prepareStatement.setInt(2, etiqueta.getArticulo());
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return ok;

}

    public boolean actualizarArticulo(Articulo articulo){
        boolean ok =false;

        Connection con = null;
        try {
            String query = "UPDATE ARTICULO SET TITULO=?, CUERPO=?, FECHA_PUBLICACION=?, AUTOR=? WHERE CODIGO = ?";
            con = DataBaseServices.getInstance().getConexion();
            PreparedStatement prepareStatement = con.prepareStatement(query);
            //Antes de ejecutar seteo los parametros.
            prepareStatement.setString(1, articulo.getTitulo());
            prepareStatement.setString(2, articulo.getCuerpo());
            prepareStatement.setDate(3, articulo.getFecha());
            prepareStatement.setString(4, articulo.getAutor());
            prepareStatement.setInt(5, articulo.getCodigo());
            //Indica el where...
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return ok;
    }

    public boolean borrarComentario(int codigoComentario) {
        boolean ok = false;

        Connection con = null;

        try {
            con = DataBaseServices.getInstance().getConexion();
            String queryComentarios = "DELETE FROM COMENTARIO WHERE CODIGO = ?";
            PreparedStatement preparedStatementComentarios = con.prepareStatement(queryComentarios);
            preparedStatementComentarios.setInt(1, codigoComentario);
            int fila = preparedStatementComentarios.executeUpdate();
            ok = fila > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ok;
    }

    public boolean borrarArticulo(int codigoArticulo){
        boolean ok =false;

        Connection con = null;
        try {
            con = DataBaseServices.getInstance().getConexion();
            String queryComentarios = "DELETE FROM COMENTARIO WHERE ARTICULO = ?";
            PreparedStatement preparedStatementComentarios = con.prepareStatement(queryComentarios);
            preparedStatementComentarios.setInt(1, codigoArticulo);
            preparedStatementComentarios.executeUpdate();

            String queryEtiqueta = "DELETE FROM ETIQUETA WHERE ARTICULO = ?";
            PreparedStatement preparedStatementEtiqueta = con.prepareStatement(queryEtiqueta);
            preparedStatementEtiqueta.setInt(1, codigoArticulo);
            preparedStatementEtiqueta.executeUpdate();

            String query = "DELETE FROM ARTICULO WHERE CODIGO = ?";
            //
            PreparedStatement prepareStatement = con.prepareStatement(query);

            //Indica el where...
            prepareStatement.setInt(1, codigoArticulo);
            //
            int fila = prepareStatement.executeUpdate();
            ok = fila > 0 ;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            try {
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ok;
    }

}
