package com.practica3.pucmm.main;


import com.practica3.pucmm.clases.Articulo;
import com.practica3.pucmm.clases.Comentario;
import com.practica3.pucmm.clases.Etiqueta;
import com.practica3.pucmm.clases.Usuario;
import com.practica3.pucmm.services.ArticuloServices;
import com.practica3.pucmm.services.DataBaseServices;
import com.practica3.pucmm.services.InicioServices;
import com.practica3.pucmm.services.UsuarioServices;
import freemarker.template.Configuration;
import org.jasypt.util.text.StrongTextEncryptor;
import spark.ModelAndView;
import spark.Request;
import spark.Session;
import spark.Spark;
import spark.template.freemarker.FreeMarkerEngine;
import static spark.Spark.*;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    private static String contrasenia = "practica3";

    public static void main(String[] args) throws SQLException {
        InicioServices.iniciarDb();
        DataBaseServices.getInstance().testConexion();
        InicioServices.crearTablas();

        Configuration configuration=new Configuration(Configuration.getVersion());
        configuration.setClassForTemplateLoading(Main.class, "/templates");
        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine(configuration);

        Spark.get("/", (request, response) -> {
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Ingresar Estudiante");
            ArticuloServices articuloServices = new ArticuloServices();
            ArrayList<Articulo> listaArticulos = articuloServices.listaArticulos();
            attributes.put("articulos", listaArticulos);
            attributes.put("esAdmin", Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador"))));
            attributes.put("usuario", String.valueOf(textEncryptor.decrypt(request.cookie("usuario"))));
            return new ModelAndView(attributes, "listaArticulos.ftl");
        }, freeMarkerEngine);

        Spark.get("/administrador/:usuario", (request, response) -> {
            String usuario = request.params("usuario");
            UsuarioServices usuarioServices = new UsuarioServices();
            Usuario usuarioAdmin = usuarioServices.getUsuario(usuario);

            Map<String, Object> attributes = new HashMap<>();
            attributes.put("usuarioAdmin", usuarioAdmin);

            if(!usuarioAdmin.isAdministrador()) {
                response.redirect("/");
                return null;
            }
            return new ModelAndView(attributes, "administrador.ftl");
        }, freeMarkerEngine);
        Spark.post("/agregarNuevoUsuario/", (request, response) -> {
            Usuario usuario = new Usuario();
            //System.out.println(request.body());
            usuario.setNombreUsuario(request.queryParams("usuario"));
            usuario.setPassword(request.queryParams("contrasenia"));
            usuario.setNombre(request.queryParams("nombre"));
            usuario.setAdministrador((request.queryParams("administrador") != null)?true:false);
            usuario.setAutor((request.queryParams("autor") != null)?true:false);
            UsuarioServices usuarioServices = new UsuarioServices();
            usuarioServices.crearUsuario(usuario);
            return new ModelAndView(null, "administrador.ftl");
        }, freeMarkerEngine);
        Spark.get("/crearArticulo/", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("titulo", "Crear Atributo");
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);

            String usuario = textEncryptor.decrypt(request.cookie("usuario"));
            boolean esAdministrador = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador")));
            boolean esAutor = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor")));
            if (esAutor == false) {
                response.redirect("/");
            }
            return new ModelAndView(attributes, "crearArticulo.ftl");
        }, freeMarkerEngine);
        Spark.get("/cargarLogin/", (request, response) -> {
            return modelAndView(null, "index.ftl");
        }, freeMarkerEngine);
        Spark.post("/login/", (request, response) -> {
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            UsuarioServices usuarioServices = new UsuarioServices();
            Usuario user = usuarioServices.getUsuario(request.queryParams("usuario"));
            String userEncripted = textEncryptor.encrypt(user.getNombreUsuario());
            String userAdmin = textEncryptor.encrypt(String.valueOf(user.isAdministrador()));
            String userAutor = textEncryptor.encrypt(String.valueOf(user.isAutor()));
            Map<String, String> cookies=request.cookies();
            String usuario = request.cookie("usuario");
            boolean esAutor = Boolean.valueOf(request.cookie("esAutor"));
            boolean esAdministrador = Boolean.valueOf(request.cookie("esAdministrador"));
            System.out.println(request.queryParams("colocarCookieDeMantenerSesion"));
            int esMantenerSesion = (request.queryParams("colocarCookieDeMantenerSesion") != null?86400:1000);
            if (user != null && usuario == null) {
                response.cookie("/", "usuario", userEncripted, esMantenerSesion, false);
                response.cookie("/", "esAdministrador", userAdmin, esMantenerSesion, false);
                response.cookie("/", "esAutor", userAutor, esMantenerSesion, false);
                System.out.println(user.getNombre());
                ///Map<String, Object> attributes = new HashMap<>();
                // attributes.put("titulo", attributes);
                response.redirect("/");
            } else if(usuario != null) {
                Map<String, Object> attributes = new HashMap<>();
                attributes.put("titulo", attributes);
                response.redirect("/");
            }
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        Spark.post("/agregarArticulo/", (request, response) -> {
            Articulo articulo = new Articulo();
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            String usuario = textEncryptor.decrypt(request.cookie("usuario"));
            boolean esAdministrador = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador")));
            boolean esAutor = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor")));
            if (!esAutor) {
                response.redirect("/");
            }
            articulo.setAutor(usuario);
            articulo.setFecha(Date.valueOf(LocalDate.now()));
            articulo.setTitulo(request.queryParams("titulo"));
            articulo.setCuerpo(request.queryParams("cuerpoArticulo"));
            System.out.println(articulo.getCuerpo());
            ArticuloServices articuloServices = new ArticuloServices();
            int codigo = articuloServices.crearArticulo(articulo);
            String etiquetas = request.queryParams("etiquetas");
            String etiquetasDiv[] = etiquetas.split(",");
            for (String etiqueta: etiquetasDiv) {
                Etiqueta etiquetaAux = new Etiqueta();
                etiquetaAux.setEtiqueta(etiqueta);
                etiquetaAux.setArticulo(codigo);
                articuloServices.crearEtiqueta(etiquetaAux);
            }
            Map<String, Object> attributes = new HashMap<>();
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        Spark.get("/articulo/:codigoArticulo", (request, response) -> {
            Articulo articulo = new Articulo();
            int codigo = Integer.valueOf(request.params("codigoArticulo"));

            ArticuloServices articuloServices = new ArticuloServices();
            articulo = articuloServices.getArticulo(codigo);
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("articulo", articulo);
            attributes.put("esAdministrador", Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador"))));
            attributes.put("esAutor", Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor"))));
            attributes.put("usuario", (textEncryptor.decrypt(request.cookie("usuario")) != null)?true:false);
            attributes.put("esMismoAutor", (articulo.getAutor() == textEncryptor.decrypt(request.cookie("usuario"))));
            return  modelAndView(attributes, "articulo.ftl");
        }, freeMarkerEngine);

        Spark.post("/comentar/:codigoArticulo", (request, response) -> {
            int codigo = Integer.valueOf(request.params("codigoArticulo"));
            String comentario = request.queryParams("comentario");
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            String usuario = textEncryptor.decrypt(request.cookie("usuario"));
            if (usuario != null) {
                Comentario coment = new Comentario();
                coment.setComentario(comentario);
                coment.setAutor(usuario);
                ArticuloServices articuloServices = new ArticuloServices();
                articuloServices.crearComentario(coment, codigo);
                response.redirect("/articulo/" + String.valueOf(codigo));
            }
            response.redirect("/articulo/" + String.valueOf(codigo));
            return null;
        }, freeMarkerEngine);
        Spark.get("/editarArticulo/:codigoArticulo", (request, response) -> {
            boolean estaLogged = filtroSiEsAdmin(request);
            if (estaLogged == false) {
                response.redirect("/");
            }
            int codigo = Integer.valueOf(request.params("codigoArticulo"));
            ArticuloServices articuloServices = new ArticuloServices();
            Articulo articulo = articuloServices.getArticulo(codigo);
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("articulo", articulo);
            return modelAndView(attributes,"editar.ftl");
        }, freeMarkerEngine);
        Spark.post("/agregarEdicionArticulo/:codigoArticulo", (request, response) -> {
            boolean estaLogged = filtroSiEsAdmin(request);
            if (estaLogged == false) {
                response.redirect("/");
            }
            int codigo = Integer.valueOf(request.params("codigoArticulo"));
            System.out.println(codigo);
            ArticuloServices articuloServices = new ArticuloServices();
            Articulo articulo = articuloServices.getArticulo(codigo);
            articulo.setCuerpo(request.queryParams("cuerpoArticulo"));
            articulo.setTitulo(request.queryParams("titulo"));
            articuloServices.actualizarArticulo(articulo);
            System.out.println("Pasó por aquí");
            response.redirect("/articulo/" + String.valueOf(articulo.getCodigo()));
            return null;
        }, freeMarkerEngine);
        Spark.get("/eliminar/:codigoArticulo", (request, response) -> {
            boolean estaLogged = filtroSiEsAdmin(request);
            if (estaLogged == false) {
                response.redirect("/");
            }
            int codigo = Integer.valueOf(request.params("codigoArticulo"));
            ArticuloServices articuloServices = new ArticuloServices();
            articuloServices.borrarArticulo(codigo);
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        Spark.get("/eliminarComentario/:codigoComentario/:codigoArticulo", (request, response) -> {
            ArticuloServices articuloServices = new ArticuloServices();
            articuloServices.borrarComentario(Integer.valueOf(request.params("codigoComentario")));
            response.redirect("/articulo/" + String.valueOf(request.params("codigoArticulo")));
            return null;
        }, freeMarkerEngine);
        Spark.post("/agregarArticuloAdmin/", (request, response) -> {
            Articulo articulo = new Articulo();
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            String usuario = textEncryptor.decrypt(request.cookie("usuario"));
            boolean esAdministrador = Boolean.valueOf(textEncryptor.decrypt((request.cookie("esAdministrador"))));
            boolean esAutor = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor")));
            if (!esAutor) {
                response.redirect("/");
            }
            articulo.setAutor(usuario);
            articulo.setFecha(Date.valueOf(LocalDate.now()));
            articulo.setTitulo(request.queryParams("titulo"));
            articulo.setCuerpo(request.queryParams("cuerpoArticulo"));
            System.out.println(articulo.getCuerpo());
            ArticuloServices articuloServices = new ArticuloServices();
            int codigo = articuloServices.crearArticulo(articulo);
            String etiquetas = request.queryParams("etiquetas");
            String etiquetasDiv[] = etiquetas.split(",");
            for (String etiqueta: etiquetasDiv) {
                Etiqueta etiquetaAux = new Etiqueta();
                etiquetaAux.setEtiqueta(etiqueta);
                etiquetaAux.setArticulo(codigo);
                articuloServices.crearEtiqueta(etiquetaAux);
            }
            Map<String, Object> attributes = new HashMap<>();
            response.redirect("/administrador/" + usuario);
            return null;
        }, freeMarkerEngine);
        Spark.get("/Logout/", (request, response) -> {
            String usuario = request.cookie("usuario");
            String esAdmin = request.cookie("esAdministrador");
            String esAutor = request.cookie("esAutor");

            response.removeCookie("/", "usuario");
            response.removeCookie("/","esAdministrador");
            response.removeCookie("/","esAutor");
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
    }
    public static boolean filtroSiEsAdmin(Request request) {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);
        textEncryptor.setPassword(contrasenia);
        String usuario = textEncryptor.decrypt(request.cookie("usuario"));
        boolean esAdministrador = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador")));
        boolean esAutor = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor")));
        if (!esAutor || !esAdministrador) {
            return false;
        }
        return true;
    }
}