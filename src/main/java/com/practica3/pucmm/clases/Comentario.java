package com.practica3.pucmm.clases;


public class Comentario {
    private String comentario;
    private String autor;
    private Articulo articulo = new Articulo();
    private int codigo;

    /**
     *
     * @param comentario
     * @param autor
     * @param codigo
     */
    public Comentario(String comentario, String autor, int codigo) {
        this.comentario = comentario;
        this.autor = autor;
        this.codigo = codigo;
    }

    public Comentario() { }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

}
