package com.practica3.pucmm.clases;

import java.util.ArrayList;
import java.sql.Date;

public class Articulo {
    private int codigo;
    private String titulo;
    private String cuerpo;
    private String autor;
    private Date fecha;
    private ArrayList<Comentario> comentarios = new ArrayList<Comentario>();
    private ArrayList<Etiqueta> etiquetas = new ArrayList<Etiqueta>();

    /**
     *
     * @param codigo
     * @param titulo
     * @param cuerpo
     * @param autor
     * @param fecha
     */
    public Articulo(int codigo, String titulo, String cuerpo, String autor, Date fecha){
        this.codigo = codigo;
        this.titulo = titulo;
        this.cuerpo = cuerpo;
        this.autor = autor;
        this.fecha = fecha;
    }
    public Articulo(){}

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public ArrayList<Etiqueta> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(ArrayList<Etiqueta> etiquetas) {
        this.etiquetas = etiquetas;
    }
}
